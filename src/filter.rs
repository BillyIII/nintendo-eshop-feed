
use sexp::{ Sexp, Atom };

use crate::select::{ Filter };
use crate::types::{ Result };


pub fn parse_filter(text: &str) -> Result<Filter> {
    parse_sexp(&sexp::parse(text)?)
}

fn parse_sexp(expr: &Sexp) -> Result<Filter> {
    match expr {
        Sexp::List(ref l) => match &l[..] {
            [Sexp::Atom(Atom::S(op)), Sexp::Atom(Atom::S(name)), Sexp::Atom(value)] if op == ":" =>
                Ok(Filter::Field { name: Some(name.clone()), value: parse_value(value) }),
            [Sexp::Atom(Atom::S(op)), left, right] if op == "and" =>
                Ok(Filter::And { left: Box::new(parse_sexp(left)?), right: Box::new(parse_sexp(right)?) }),
            _ => Err(format_err!("Invalid expression: {}", &expr))
        },
        _ => Err(format_err!("Invalid expression: {}", &expr))
    }
}

fn parse_value(expr: &Atom) -> Option<String> {
    Some(match expr {
        Atom::S(s) => s.clone(),
        Atom::I(n) => format!("{}", n),
        Atom::F(n) => format!("{}", n),
    })
}
