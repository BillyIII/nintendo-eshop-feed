
use diesel::prelude::*;
use futures::stream::{ FuturesUnordered };
use tokio_stream::{ StreamExt };
use std::fs::{ File };
use std::io::{ Write };
use std::str::FromStr;
use chrono::{ DateTime, Utc };
use scraper::{ Html, Selector };
use regex::{ Regex };

use crate::types::{ Result };
use crate::game::{ Game };
use crate::model as m;
use crate::db::{ Db, parse_db_timestamp };
use crate::schema::games::dsl as g;
use crate::schema::discounts::dsl as d;


const NET_RETRIES : usize = 2;


pub async fn generate_discounts_feed<F>(db: &mut Db, path: &str, domain: &str, progress: F) -> Result<()>
where F: Fn(usize, usize) -> ()
{
    let mut mgames : Vec<(m::Discount, m::Game)> = db.exec(move |conn| d::discounts
                                                           .inner_join(
                                                               g::games.on(
                                                                   d::game.eq(g::id)))
                                                           .order(d::first_seen.desc())
                                                           .load(conn)).await??;

    let filets = if mgames.is_empty() {
        Utc::now()
    }
    else {
        parse_db_timestamp(mgames[0].0.first_seen)?
    };

    let total = mgames.len();

    
    let mut index : usize = 0;
    let mut futures : FuturesUnordered<_> = mgames.drain(..).map(move |(mdiscount, mgame)| {
        let resindex = index;
        index += 1;
        async move {
            let ts = parse_db_timestamp(mdiscount.first_seen)?;
            let game = Game::from_db(&mgame)?;
            
            let res = if game.has_discount {
                Some(make_entry(&game, &ts, domain).await?)
            }
            else {
                None
            };
            
            Ok((resindex, res)) : Result<(usize, Option<String>)>
        }
    }).collect();

    let mut entries = Vec::<Option<String>>::new();
    entries.resize(total, None);

    let mut current = 0;
    progress(current, total);

    while let Some(res) = futures.next().await {
        let (entindex, entry) = res?;
        entries[entindex] = entry;
        
        current += 1;
        progress(current, total);    
    }

    
    let mut tmppath = std::path::PathBuf::new();
    let ts = chrono::DateTime::<chrono::Utc>::from(std::time::SystemTime::now()).timestamp();
    tmppath.push(path);
    tmppath.set_file_name(format!("feed-tmp-{}", ts));

    {
        let mut file = File::create(&tmppath)?;
        
        write_header(&mut file, &filets).await?;

        for entry in entries {
            if let Some(ent) = entry {
                file.write(ent.as_bytes())?;
            }
        }

        write_footer(&mut file).await?;
    }

    std::fs::rename(&tmppath, path)?;

    Ok(())
}

async fn write_header(file: &mut File, ts: &DateTime<Utc>) -> Result<()> {
    file.write(b"<?xml version=\"1.0\" encoding=\"utf-8\"?> \
                 \
                 <feed xmlns=\"http://www.w3.org/2005/Atom\"> \
                 \
	             <title>Nintendo eShop Discounts</title> \
	             <updated>")?;
    write_escaped(file, &ts.to_rfc3339())?;
    file.write(b"</updated>\n")?;

    Ok(())
}

async fn write_footer(file: &mut File) -> Result<()> {
    file.write(b"</feed>")?;

    Ok(())
}

async fn make_entry(game: &Game, ts: &DateTime<Utc>, domain: &str) -> Result<String> {
    let mut res = String::new();
    
    res.push_str("<entry>\n");
    res.push_str("  <title>"); res.push_str(&escape_for_xml(&game.title)); res.push_str("</title>\n");
    
    res.push_str("  <link href=\"");
    res.push_str("https://");
    res.push_str(&escape_for_xml(domain));
    res.push_str(&escape_for_xml(&game.url));
    res.push_str("\" />\n");
    
    res.push_str("  <id>"); res.push_str(&escape_for_xml(&format!("{}", game.id))); res.push_str("</id>\n");
    res.push_str("  <updated>"); res.push_str(&escape_for_xml(&ts.to_rfc3339())); res.push_str("</updated>\n");
    res.push_str("  <summary>"); res.push_str(&escape_for_xml(&game.excerpt)); res.push_str("</summary>\n");

    res.push_str(&make_entry_content(domain, game).await?);
    
    res.push_str("</entry>\n");
    
    Ok(res)
}

async fn make_entry_content(domain: &str, game: &Game) -> Result<String> {
    let mut res = String::new();
    
    res.push_str("  <content type=\"xhtml\">\n");

    res.push_str("    <h1>"); res.push_str(&escape_for_xml(&game.title)); res.push_str("</h1>\n");
    res.push_str("    <p>"); res.push_str(&escape_for_xml(&game.excerpt)); res.push_str("</p>\n");
    res.push_str("    <p>\n");
    
    if let Some(disc) = game.price_discounted {
        res.push_str("      <span>"); res.push_str(&format_price(disc)); res.push_str("</span>\n");
        res.push_str("      <span style=\"text-decoration:line-through\">"); res.push_str(&format_price(game.price)); res.push_str("</span>\n");
    }
    else {
        res.push_str("      <span>"); res.push_str(&format_price(game.price)); res.push_str("</span>\n");
    }
    
    res.push_str("    </p>\n");

    let url = "https://".to_owned() + domain + &game.url;
    if let Ok(body) = crate::network::get(&http::uri::Uri::from_str(&url)?, NET_RETRIES).await {
        let html = Html::parse_document(&body);

        let oversel = Selector::parse("#Overview div.content").map_err(|e| format_err!("{:?}", e))?;
        if let Some(overview) = html.select(&oversel).next() {
            res.push_str(&overview.inner_html());
        }

        res.push_str("    <p>\n");

        lazy_static! {
            static ref GALEXPR: Regex = Regex::new(r"'image_url'\s*:\s*'([^']+)'").unwrap();
        }
        
        let galsel = Selector::parse("#Gallery script").map_err(|e| format_err!("{:?}", e))?;
        if let Some(gal) = html.select(&galsel).next() {
            for m in GALEXPR.captures_iter(&gal.inner_html()) {
                res.push_str("      <img src=\"");
                res.push_str(&m[1]);
                res.push_str("\" />\n");
            }
        }
        
        res.push_str("    </p>\n");
    }
    
    res.push_str("  </content>\n");

    Ok(res)
}

fn format_price(price: u64) -> String {
    format!("{:01}.{:02}", price / 100, price % 100)
}
    
fn escape_for_xml(s: &str) -> String {
    s   .replace('&', "&amp;")
        .replace('<', "&lt;")
        .replace('>', "&gt;")
        .replace('"', "&quot;")
        .replace('\'', "&apos;")
}

fn write_escaped(file: &mut File, s: &str) -> Result<()> {
    file.write(escape_for_xml(s).as_bytes())?;
    Ok(())
}
