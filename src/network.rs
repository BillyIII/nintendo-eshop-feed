
use isahc::prelude::*;

use crate::types::{ Error, Result };


pub async fn get_internal(uri: &http::uri::Uri, retries: usize) -> Result<String> {
    let mut left = retries;
    loop {
        match isahc::get_async(uri).await?.text().await {
            Ok(ok) => return Ok(ok),
            Err(err) => {
                if left == 0 {
                    return Err(Error::from(err));
                }
                else {
                    left -= 1;
                }
            },
        }
    }
}

#[cfg(feature = "cache")]
pub async fn get(uri: &http::uri::Uri, retries: usize) -> Result<String> {
    let mut path = std::path::PathBuf::new();
    path.push("./cache");
    path.push(uri.to_string().replace("&", "/").replace("?", "/"));
    std::fs::create_dir_all(path.parent().ok_or_else(|| format_err!("select::do_request_internal::path.parent"))?)?;
    if let Ok(bytes) = std::fs::read(&path) {
        Ok(String::from_utf8_lossy(&bytes).into_owned())
    }
    else {
        let res = get_internal(uri, retries).await?;
        std::fs::write(&path, res.as_bytes())?;
        Ok(res)
    }
}

#[cfg(not(feature = "cache"))]
pub async fn get(uri: &http::uri::Uri, retries: usize) -> Result<String> {
    get_internal(uri, retries).await
}
