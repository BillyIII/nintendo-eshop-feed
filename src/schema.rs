table! {
    discounts (game) {
        game -> Integer,
        first_seen -> BigInt,
    }
}

table! {
    games (id) {
        id -> Integer,
        ctime -> BigInt,
        mtime -> BigInt,
        title -> Text,
        url -> Text,
        excerpt -> Text,
        has_discount -> Integer,
        price -> BigInt,
        price_discounted -> Nullable<BigInt>,
        json -> Text,
    }
}

joinable!(discounts -> games (game));

allow_tables_to_appear_in_same_query!(
    discounts,
    games,
);
