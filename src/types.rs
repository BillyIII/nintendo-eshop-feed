
pub type Error = failure::Error;
pub type Result<T> = std::result::Result<T, Error>;

pub type DbId = i32;
pub type DbIdSql = diesel::sql_types::Integer;

pub type DbConn = diesel::SqliteConnection;
pub type Db = diesel::sqlite::Sqlite;
