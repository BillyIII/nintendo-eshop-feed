
#![feature(async_closure)]
#![feature(future_poll_fn)]
#![feature(type_ascription)]

#[macro_use]
extern crate failure;
#[macro_use]
extern crate diesel;
// #[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate lazy_static;

// embed_migrations!();
    // let db = DbConn::establish(uri)?;

    // embedded_migrations::run_with_output(&db, &mut std::io::stderr())?;

pub mod types;
pub mod db;
pub mod select;
pub mod schema;
pub mod model;
pub mod game;
pub mod urls;
pub mod network;
pub mod filter;
pub mod update;
pub mod feed;
