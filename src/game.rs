
use json::{ JsonValue };
use chrono::{ DateTime, Utc };

use crate::types::{ Result, DbId };
use crate::model as m;
use crate::db::{ parse_db_timestamp };


pub struct Game {
    pub id: DbId,
    pub ctime: DateTime<Utc>,
    pub mtime: DateTime<Utc>,
    pub title: String,
    pub url: String,
    pub excerpt: String,
    pub has_discount: bool,
    pub price: u64,
    pub price_discounted: Option<u64>,
    pub json: JsonValue,
}

impl Game {
    pub fn from_json(json: JsonValue) -> Result<Game> {
        let id = json["fs_id"].as_str().ok_or_else(|| format_err!("Game::from_json: Missing id"))?.parse()?;
        let ctime = parse_json_timestamp(&json["dates_released_dts"][0])?;
        let mtime = parse_json_timestamp(&json["change_date"])?;
        let title = json["title"].as_str().ok_or_else(|| format_err!("Game::from_json: Missing title"))?.parse()?;
        let url = json["url"].as_str().ok_or_else(|| format_err!("Game::from_json: Missing url"))?.parse()?;
        let excerpt = json["excerpt"].as_str().ok_or_else(|| format_err!("Game::from_json: Missing excerpt"))?.parse()?;
        let has_discount = json["price_has_discount_b"].as_bool().ok_or_else(|| format_err!("Game::from_json: Missing has_discount"))?;
        let price = parse_price(&json["price_regular_f"])?;
        let price_discounted = if has_discount {
            Some(parse_price(&json["price_discounted_f"])?)
        }
        else {
            None
        };

        Ok(Game { id, ctime, mtime, title, url, excerpt, has_discount, price, price_discounted, json })
    }

    pub fn from_db(model: &m::Game) -> Result<Game> {
        Ok(Game {
            id: model.id,
            ctime: parse_db_timestamp(model.ctime)?,
            mtime: parse_db_timestamp(model.mtime)?,
            title: model.title.clone(),
            url: model.url.clone(),
            excerpt: model.excerpt.clone(),
            has_discount: model.has_discount != 0,
            price: model.price as u64,
            price_discounted: model.price_discounted.map(|n| n as u64),
            json: json::parse(&model.json)?,
        })
    }

    pub fn to_db(&self) -> Result<m::Game> {
        Ok(m::Game {
            id: self.id,
            ctime: self.ctime.timestamp(),
            mtime: self.mtime.timestamp(),
            title: self.title.clone(),
            url: self.url.clone(),
            excerpt: self.excerpt.clone(),
            has_discount: if self.has_discount { 1 } else { 0 },
            price: self.price as i64,
            price_discounted: self.price_discounted.map(|n| n as i64),
            json: self.json.dump(),
        })
    }
}


fn parse_json_timestamp(v: &JsonValue) -> Result<DateTime<Utc>> {
    let ts = v.as_str().ok_or_else(|| format_err!("Game::parse_json_timestamp: Missing time"))?;
    Ok(chrono::DateTime::parse_from_rfc3339(ts)?.with_timezone(&Utc))
}

fn parse_price(v: &JsonValue) -> Result<u64> {
    v   .as_number()
        .ok_or_else(|| format_err!("Game::parse_price: Missing price"))?
        .as_fixed_point_u64(2)
        .ok_or_else(|| format_err!("Game::parse_price: Invalid price"))
}
