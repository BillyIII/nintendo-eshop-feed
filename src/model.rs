
use diesel::prelude::*;

// use crate::schema::docs::dsl as docs;
use crate::schema::games;
use crate::schema::discounts;
use crate::types::{ DbId };


#[derive(Clone, Queryable, Insertable, QueryableByName, AsChangeset)]
#[table_name = "games"]
pub struct Game {
    pub id: DbId,
    pub ctime: i64,
    pub mtime: i64,
    pub title: String,
    pub url: String,
    pub excerpt: String,
    pub has_discount: i32,
    pub price: i64,
    pub price_discounted: Option<i64>,
    pub json: String,
}

#[derive(Clone, Queryable, Insertable, QueryableByName, AsChangeset)]
#[table_name = "discounts"]
pub struct Discount {
    pub game: DbId,
    pub first_seen: i64,
}
