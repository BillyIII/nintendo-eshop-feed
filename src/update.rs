
use chrono::{ Utc };
use diesel::prelude::*;
use futures::stream::{ FuturesUnordered };
use tokio_stream::{ StreamExt };

use crate::types::{ Result };
use crate::select::{ Filter, Sort, SortDir, do_request_all };
use crate::game::{ Game };
use crate::db::{ Db };
use crate::schema::games::dsl as g;
// use crate::schema::games;


const REQUEST_RETRIES : usize = 3;

// TODO: Pagination.
// TODO: Prices.
pub async fn update_games<F>(db: &Db, domain: &str, lang: &str, query: Option<String>, filter: Filter, progress: F)
                             -> Result<Vec<Game>>
where F: Fn(usize, usize) -> ()                          
{
    let sort = vec!(Sort { field: "popularity".to_owned(), dir: SortDir::Asc });
    let mut jsons = do_request_all(domain, lang, query, filter, sort, REQUEST_RETRIES).await?;
    
    let total = jsons.len();
    
    let mut futures : FuturesUnordered<_> = jsons.drain(..).map(async move |json| {
        let game = Game::from_json(json)?;

        let dbgame = game.to_db()?;
        db.exec(move |conn| {
            // let updres = diesel::update(g::games.find(dbgame.id))
            //     .set(&dbgame)
            //     .execute(conn);
            // if let Ok(0) = updres {
            //     diesel::insert_into(g::games)
            //         .values(&dbgame)
            //         .execute(conn)
            // }
            // else {
            //     updres
            // }
            // println!("QUERY");
            // println!("{}", diesel::debug_query::<diesel::sqlite::Sqlite, _>(
            //     &diesel::insert_into(g::games)
            //         .values(&dbgame)
            //         .on_conflict(g::id)
            //         .do_update()
            //         .set(&dbgame)));
            // println!("\nEND");
            diesel::insert_into(g::games)
                .values(&dbgame)
                .on_conflict(g::id)
                .do_update()
                .set(&dbgame)
                .execute(conn)
            // Ok(()) : Result<()>
        }).await??;

        Ok(game) : Result<Game>
    }).collect();

    let mut games = Vec::new();
    
    progress(0, total);
    while let Some(res) = futures.next().await {
        games.push(res?);
        progress(games.len(), total);
    }

    Ok(games)
}

pub async fn update_discounts(db: &mut Db) -> Result<()> {
    let delete_query = "delete from discounts where game not in \
                        (select id from games where has_discount = true);";
    let insert_query = "insert into discounts(game, first_seen) \
                        select id, ? from games where has_discount = true \
                        on conflict(game) do nothing;";
    db.exec(move |conn| {
        diesel::sql_query(delete_query)
            .execute(conn)?;
        diesel::sql_query(insert_query)
            .bind::<diesel::sql_types::BigInt, _>(Utc::now().timestamp())
            .execute(conn)
    }).await??;

    Ok(())
}

pub async fn reset_discounts(db: &mut Db) -> Result<()> {
    db.exec(move |conn| {
        diesel::update(g::games)
            .set(g::has_discount.eq(0))
            .execute(conn)
    }).await??;

    Ok(())
}
