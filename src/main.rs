
use structopt::StructOpt;

use nintendo_eshop_feed::types::{ Result };
use nintendo_eshop_feed::db::{ Db };
use nintendo_eshop_feed::filter::{ parse_filter };
use nintendo_eshop_feed::urls::{ EUROPEAN_SEARCH_DOMAIN, RUSSIAN_DOMAIN };
use nintendo_eshop_feed::update;
use nintendo_eshop_feed::feed;


#[derive(Debug, StructOpt)]
struct Options {
    /// Database path.
    #[structopt(short, long, default_value="db.sqlite3")]
    db: String,
    #[structopt(short, long, default_value="feed.xml")]
    feed: String,
    // #[structopt(short, long, default_value="pages")]
    // pages: String,
    #[structopt(short, long, default_value="ru")]
    lang: String,
}

#[tokio::main]
async fn main() {
    let opt = Options::from_args();

    if let Err(err) = do_options(&opt).await {
        eprintln!("Error: {:?}", err);
        std::process::exit(1);
    }
}

async fn do_options(options: &Options) -> Result<()> {
    let mut db = Db::new(nintendo_eshop_feed::db::connect(&options.db)?);
    let tty = atty::is(atty::Stream::Stdout);

    println!("Retreiving new entries.");

    update::reset_discounts(&mut db).await?;
    
    let filter = parse_filter("(and (and (: type GAME) (: playable_on_txt \"HAC\")) (: price_has_discount_b true))")?;
    update::update_games(&mut db, EUROPEAN_SEARCH_DOMAIN, &options.lang, None, filter, |n,t| {
        if tty {
            print!("\x1B[2K\rProcessed {}/{}...", n, t);
        }
    }).await?;
    
    if tty {
        print!("\x1B[2K\r");
    }

    println!("Updating discounts.");
    
    update::update_discounts(&mut db).await?;

    println!("Generating feed.");
    
    feed::generate_discounts_feed(&mut db, &options.feed, RUSSIAN_DOMAIN, |n,t| {
        if tty {
            print!("\x1B[2K\rProcessed {}/{}...", n, t);
        }
    }).await?;
    
    if tty {
        print!("\x1B[2K\r");
    }

    println!("Done.");

    Ok(())
}
