
use diesel::prelude::*;
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use tokio::sync::{ mpsc, oneshot };
use chrono::{ DateTime, NaiveDateTime, Utc };

use crate::types::{ Error, Result };
pub use crate::types::{ DbId, DbConn };

const DATABASE_WORKER_CHANNEL_SIZE : usize = 1;


pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("migrations");

pub fn connect(uri: &str) -> Result<DbConn> {
    let mut db = DbConn::establish(uri)?;

    db.run_pending_migrations(MIGRATIONS).map_err(|e| format_err!("{}", e))?;

    diesel::sql_query("PRAGMA foreign_keys = ON;").execute(&mut db)?;

    Ok(db)
}


type DbSendFunc = dyn FnOnce(&mut DbConn) -> () + Send + 'static;
type DbSendVal = Box<DbSendFunc>;

#[derive(Clone)]
pub struct Db {
    sender: mpsc::Sender<DbSendVal>,
}

impl Db {
    pub fn new(mut db: DbConn) -> Self {
        let (sender, mut receiver)  = mpsc::channel::<DbSendVal>(DATABASE_WORKER_CHANNEL_SIZE);

        std::thread::spawn(move || {
            while let Some(job) = receiver.blocking_recv() {
                job(&mut db);
            }
        });
        
        Db { sender }
    }

    pub async fn exec<R, F>(&self, func: F) -> Result<R>
    where R: Send + 'static,
          F: FnOnce(&mut DbConn) -> R + Send + 'static
    {
        let (rsnd, rrcv) = oneshot::channel::<R>();
        let func = Box::new(func);
        self.sender.send(Box::new(move |db| {
            rsnd.send(func(db)).unwrap_or_else(|_| panic!("Databasa::exec::rsnd"));
        })).await.map_err(|_| format_err!("Databasa::exec::sender"))?;
        rrcv.await.map_err(Error::from)
    }
}

pub fn parse_db_timestamp(v: i64) -> Result<DateTime<Utc>> {
    let ndt = NaiveDateTime::from_timestamp_opt(v, 0).ok_or_else(|| format_err!("db::parse_db_timestamp: Invalid time"))?;
    Ok(chrono::DateTime::from_utc(ndt, Utc))
}
