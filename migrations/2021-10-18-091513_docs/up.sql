
create table games(
    id integer not null primary key,
    ctime bigint not null,
    mtime bigint not null,
    title text not null,
    url text not null,
    excerpt text not null,
    has_discount integer not null,
    price bigint not null,
    price_discounted bigint null,
    json text not null
);

create index games_has_discount on games(has_discount);


create table discounts(
    game integer not null primary key references games(id) on delete cascade,
    first_seen bigint not null
);

create index discounts_first_seen on discounts(first_seen);
