
use crate::types::{ Error, Result };


const NET_RETRIES : usize = 0;


pub enum Filter {
    Field {
        name: Option<String>,
        value: Option<String>,
    },
    And {
        left: Box<Filter>,
        right: Box<Filter>,
    },
}

impl Default for Filter {
    fn default() -> Filter {
        Filter::Field { name: None, value: None }
    }
}

pub enum SortDir {
    Asc,
    Desc,
}

impl Default for SortDir {
    fn default() -> SortDir {
        SortDir::Asc
    }
}

#[derive(Default)]
pub struct Sort {
    pub field: String,
    pub dir: SortDir,
}

#[derive(Default)]
pub struct Select {
    pub query: Option<String>,
    pub filter: Filter,
    pub sort: Vec<Sort>,
    pub start: usize,
    pub rows: usize,
}


// https://searching.nintendo-europe.com/ru/select?q=*&fq=type:GAME AND ((playable_on_txt:"HAC") AND (price_has_discount_b:"true")) AND sorting_title:* AND *:*&sort=deprioritise_b asc, popularity asc&start=48&rows=24&wt=json&bf=linear(ms(priority,NOW/HOUR),1.1e-11,0)&bq=!deprioritise_b:true^999&json.wrf=nindo.net.jsonp.jsonpCallback_1489371

pub fn format_uri(domain: &str, lang: &str, select: &Select) -> Result<http::uri::Uri> {
    http::uri::Builder::new()
        .scheme("https")
        .authority(domain)
        .path_and_query(make_path_and_query(lang, select)?)
        .build()
        .map_err(Error::from)
}

fn make_path_and_query(lang: &str, select: &Select) -> Result<http::uri::PathAndQuery> {
    let mut res : String = "/".to_owned() + lang + "/select?wt=json";

    push_str_arg(&mut res, "q", select.query.clone().unwrap_or("*".to_owned()));
    push_str_arg(&mut res, "fq", format_filter(&select.filter));
    push_str_arg(&mut res, "sort", format_sort(&select.sort));
    push_num_arg(&mut res, "start", select.start);
    push_num_arg(&mut res, "rows", select.rows);

    http::uri::PathAndQuery::try_from(&res).map_err(Error::from)
}

fn push_str_arg(query: &mut String, name: &str, value: String) {
    query.push_str("&");
    query.push_str(name);
    query.push_str("=");
    query.push_str(&escape_query(&value));
}

fn push_num_arg(query: &mut String, name: &str, value: usize) {
    query.push_str("&");
    query.push_str(name);
    query.push_str("=");
    query.push_str(&format!("{}", value));
}

fn escape_query(q: &str) -> String {
    let mut res = String::new();
    for b in q.as_bytes() {
        res.push_str(&format!("%{:02X}", b));
    }
    res
}

fn format_filter(filter: &Filter) -> String {
    match filter {
        Filter::Field { name, value } =>
            name.clone().unwrap_or("*".to_owned())
            + ":" + &value.clone().unwrap_or("*".to_owned()),
        Filter::And { left, right } =>
            "(".to_owned() + &format_filter(&left) + " AND " + &format_filter(&right) + ")",
    }
}

fn format_sort(sort: &Vec<Sort>) -> String {
    sort.iter()
        .map(|s| String::new() + &s.field + " " + match s.dir { SortDir::Asc => "asc", SortDir::Desc => "desc" })
        .intersperse(", ".to_owned()).collect()
}

pub struct SelectResult {
    pub num_found: usize,
    pub start: usize,
    pub docs: Vec<json::JsonValue>,
}

pub fn parse_response(body: &str) -> Result<SelectResult> {
    let mut response = json::parse(body)?["response"].take();
    let num_found = response["numFound"].as_usize().ok_or_else(|| format_err!("Missing numFound"))?;
    let start = response["start"].as_usize().ok_or_else(|| format_err!("Missing start"))?;
    let docs = match response["docs"].take() {
        json::JsonValue::Array(v) => Ok(v),
        _ => Err(format_err!("Invalid docs field"))
    }?;
    
    Ok(SelectResult { num_found, start, docs })
}

pub async fn do_request(domain: &str, lang: &str, select: &Select) -> Result<SelectResult> {
    parse_response(&crate::network::get(&format_uri(domain, lang, select)?, NET_RETRIES).await?)
}

pub async fn do_request_all(domain: &str, lang: &str, query: Option<String>, filter: Filter, sort: Vec<Sort>, retries: usize)
                      -> Result<Vec<json::JsonValue>>
{
    let mut req = Select {
        query, filter, sort,
        start: 0, rows: 0
    };

    let mut retries_left = retries;

    loop {
        req.rows = 0;        
        let SelectResult { num_found, .. } = do_request(domain, lang, &req).await?;

        req.rows = num_found;
        let res = do_request(domain, lang, &req).await?;

        if res.num_found == num_found {
            return Ok(res.docs);
        }
        else if retries_left > 0 {
            retries_left -= 1;
        }
        else {
            return Err(format_err!("select::do_request_all: Out of retries"));
        }
    }
}
